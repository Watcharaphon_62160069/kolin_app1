package com.watcharaphon.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.util.Log
import android.widget.Button
;
import android.widget.TextView
import kotlin.math.log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btn = findViewById<Button>(R.id.btn_hello)
        val name = findViewById<TextView>(R.id.txtName)
        val sentName = name.text.toString()

        btn.setOnClickListener {
            val Int = Intent(this,HelloActivity::class.java).apply {
                putExtra(EXTRA_MESSAGE, sentName)
                Log.d("Name", sentName)
            }
            startActivity(Int)
        }
    }
}